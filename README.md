# CRYPT - API

Cette API, a pour objectif de crypter des chaînes de caractères passé dans le body des requêtes HTTP.

## Documentation Swagger

Une documentation est disponible à l'URL suivante : [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

## Base64 Encode

Route permettant le cryptage en base64 d'une chaîne de caractères

```bash
curl -H "Content-Type: text/plain" -X POST -d 'test' http://localhost:8080/encode/base64
```

## SHA - 256 Encode

Route permettant le hachage en SHA-256 d'une chaîne de caractères

```bash
curl -H "Content-Type: text/plain" -X POST -d 'test' http://localhost:8080/encode/sha256
```

## Bcrypt Encode

Route permettant le cryptage grâce à la fonction Bcrypt de Spring

```bash
curl -H "Content-Type: text/plain" -X POST -d 'test' http://localhost:8080/encode/bcrypt?strength=14
```

## MD4 Encode
Route permettant le hachage en MD4 d'une chaîne de caractères

```bash
curl -H "Content-Type: text/plain" -X POST -d 'test' http://localhost:8080/encode/md4
```
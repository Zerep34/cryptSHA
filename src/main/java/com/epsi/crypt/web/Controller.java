package com.epsi.crypt.web;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.Md4PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

@RestController
@RequestMapping(path="/encode")
public class Controller {

    @PostMapping(path="/sha256", consumes = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String cryptSha(@RequestBody String str) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] hash = digest.digest(str.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(hash);
    }

    @PostMapping(path="/bcrypt", consumes = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String bCrypt(@RequestBody String str, @RequestParam int strength){
        if(strength > 31){
            strength = 31;
        }
        if (strength < 1){
            strength = 1;
        }
        return new BCryptPasswordEncoder(strength).encode(str);
    }

    @PostMapping(path="/base64", consumes = MediaType.TEXT_PLAIN_VALUE)
    public @ResponseBody String base64Encode(@RequestBody String str){
        return Base64.getEncoder().encodeToString(str.getBytes());
    }

    @PostMapping(path="/md4", consumes = MediaType.TEXT_PLAIN_VALUE)
    public  @ResponseBody String md4Encode(@RequestBody String str){
        return new Md4PasswordEncoder().encode(str);
    }
}

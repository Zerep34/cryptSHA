package com.epsi.crypt;

import com.epsi.crypt.web.Controller;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes= Controller.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@EnableAutoConfiguration
class ControllerTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    @BeforeEach
    public void setup() throws Exception{
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }

    @Test
    void cryptSha() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/encode/sha256").contentType(MediaType.TEXT_PLAIN_VALUE).content("test"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("n4bQgYhMfWWaL+qgxVrQFaO/TxsrC4Is0V1sFbDwCgg="));
    }

    @Test
    void bCrypt() throws Exception {

        mvc.perform(MockMvcRequestBuilders.post("/encode/bcrypt?strength=14").contentType(MediaType.TEXT_PLAIN_VALUE).content("test"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void base64Encode() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/encode/base64").contentType(MediaType.TEXT_PLAIN_VALUE).content("test"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("dGVzdA=="));
    }

    @Test
    void md4Encode() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/encode/md4").contentType(MediaType.TEXT_PLAIN_VALUE).content("test"))
                .andExpect(status().is2xxSuccessful());
    }
}